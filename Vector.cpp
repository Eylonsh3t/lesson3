#include <iostream>
#include <string>
#include "Vector.h"

#define MIN_SIZE 2
#define STARTING_VALUE -1
#define ERROR_NUM -9999

Vector::Vector(int n)
{
	if (n < MIN_SIZE)
	{
		n = MIN_SIZE;
	}
	this->_capacity = n;
	int* vectorArr = new int[n];
	this->_size = 0;
	this->_resizeFactor = this->_capacity;
	// insert starting values.
	for (int i = 0; i < n; i++)
	{
		vectorArr[i] = STARTING_VALUE;
	}
	this->_elements = vectorArr;
}

Vector::~Vector()
{
	delete[] this->_elements;
}

int Vector::size() const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

bool Vector::empty() const
{
	bool isEmpty = true;
	if (this->_elements)
	{
		isEmpty = false;
	}

	return isEmpty;
}

void Vector::push_back(const int& val)
{
	if(this->_size == this->_capacity)
	{
		int newSize = this->_capacity + this->_resizeFactor;
		int* tempArr = new int[newSize];
		
		// insert starting values.
		for (int i = 0; i < newSize; i++)
		{
			tempArr[i] = STARTING_VALUE;
		}
		
		for (int i = 0; i < this->_size; i++)
		{
			tempArr[i] = this->_elements[i];
		}
		this->~Vector();
		this->_elements = tempArr;
		this->_size = newSize;
	}
	int arrSize = this->size();
	this->_elements[arrSize] = val;
	(this->_size)++;
}

int Vector::pop_back()
{
	bool isEmpty = this->empty();
	if (isEmpty == true)
	{
		std::cerr << "error: pop from empty vector";
		return ERROR_NUM;
	}
	int lastValue = this->_elements[(this->_size) - 1];
	this->_elements[this->_size] = STARTING_VALUE;
	(this->_size)--;

	return lastValue;
}

void Vector::reserve(int n)
{
	if (n > this->_capacity)
	{
		do
		{
			this->_capacity += this->_resizeFactor;
		} while (n > this->_capacity);

		int* tempArr = new int[this->_capacity];
		for (int i = 0; i < this->_capacity; i++)
		{
			tempArr[i] = this->_elements[i];
		}
		this->~Vector();
		this->_elements = tempArr;
	}
}

void Vector::resize(int n)
{
	if (n <= this->_capacity)
	{
		for (int i = n; i > this->_size; i--)
		{
			int notReal = this->pop_back();
		}
	}
	else
	{
		this->reserve(n);
		for (int i = this->_size; i < n; i++)
		{
			this->_elements[i] = STARTING_VALUE;
		}
	}
			this->_size = n;
}

void Vector::assign(int val)
{
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}

void Vector::resize(int n, const int& val)
{
	if (n <= this->_capacity)
	{
		for (int i = n; i > this->_size; i--)
		{
			int notReal = this->pop_back();
		}
	}
	else
	{
		this->reserve(n);
		for (int i = this->_size; i < n; i++)
		{
			this->_elements[i] = val;
		}
	}
	this->_size = n;
}

Vector::Vector(const Vector& other)
{
	Vector newOne = operator=(other);
}

Vector& Vector::operator=(const Vector& other)
{
	Vector newOne = NULL;
	newOne._capacity = other.capacity();
	int* vectorArr = new int[newOne._capacity];
	newOne._size = other.size();
	newOne._resizeFactor = newOne._capacity;
	for (int i = 0; i < newOne._size; i++)
	{
		vectorArr[i] = other._elements[i];
	}
	newOne._elements = vectorArr;
	return newOne;
}

int& Vector::operator[](int n) const
{
	// TODO: insert return statement here
	int reference = 0;
	if (n > this->_size)
	{
		std::cerr << "n is bigger than the size of this array";
		reference = this->_elements[0];
	}
	else
	{
		reference = this->_elements[n];
	}
	return reference;
}
